const mongoose = require('mongoose')
const { PostMessage } = require('./postMessage')
const { ObjectId } = require('mongoose')

var Comments = mongoose.model('Comments',
    {
        postId: { type: ObjectId, ref: PostMessage },
        message: { type: String },
    }, 'comments')

module.exports = { Comments }