const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/postManagerDB', { useNewUrlParser: true, useUnifiedTopology: true },
    err => {
        if (!err)
            console.log('Connection Successfull.')
        else
            console.log('Error connecting database : ' + JSON.stringify(err, undefined, 2))
    })