const express = require('express')
var router = express.Router()
var ObjectID = require('mongoose').Types.ObjectId
var { Comments } = require('../models/comments')
var { PostMessage } = require('../models/postMessage')

//get
router.get('/', (req, res) => {
    PostMessage.aggregate([
        {
            $lookup: {
                from: 'comments',
                localField: '_id',
                foreignField: 'postId',
                as: 'comments'
            },
        },
    ],

        function (err, docs) {
            if (!err) {
                res.send(docs);
            } else {
                console.log('Error while retriving records : ' + err)
            }
        }
    );
})

// router.get('/', (req, res) => {
//     Comments.find().populate('postId').exec(function (err, docs) {
//         if (!err) {
//             res.send(docs);
//         } else {
//             console.log('Error while retriving records : ' + JSON.stringify(err, undefined, 2))
//         }
//     });
// });

//create
router.post('/', (req, res) => {
    var newRecord = new Comments({
        postId: ObjectID(req.body.postId),
        message: req.body.message
    })

    newRecord.save((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while creating new record : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

//delete
router.delete('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send('No record with given id : ' + req.params.id);

    Comments.findByIdAndRemove(req.params.id, (err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while deleting a record : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

module.exports = router