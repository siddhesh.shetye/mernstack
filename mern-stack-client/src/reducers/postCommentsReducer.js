import { ACTION_TYPES } from '../actions/postComments';

const initialState = {
    commentlist: []
}

export const postComments = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.COMMENT_FETCH_ALL:
            return {
                ...state,
                commentlist: [...action.payload]
            }

        case ACTION_TYPES.COMMENT_CREATE:
            var post = state.commentlist.find(x => x._id === action.payload.postId);
            post.comments.push(action.payload);

            return {
                ...state,
                commentlist: [...state.commentlist]
            }

        case ACTION_TYPES.COMMENT_DELETE:
            state.commentlist[action.payload.index].comments = state.commentlist[action.payload.index].comments.filter(x => x._id !== action.payload.id);

            return {
                ...state,
                commentlist: [...state.commentlist]
            }

        default:
            return state;
    }
}