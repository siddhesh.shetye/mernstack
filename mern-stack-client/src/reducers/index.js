import { combineReducers } from 'redux';
import { postMessage } from './postMessage';
import { postComments } from './postCommentsReducer';

export const reducers = combineReducers({
    postMessage,
    postComments
})