import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { store } from './actions/store';
import {
  HashRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import Dashboard from "./components/dashboard.js";
import Posts from "./components/Posts/index.js";
import Comments from "./components/Comments/index";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path='/' component={Dashboard} />
          <Route exact path='/posts' component={Posts} />
          <Route exact path='/comments' component={Comments} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
