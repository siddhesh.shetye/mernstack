import React from 'react';
import { TextField, Button, InputLabel, Select, MenuItem } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import useForm from '../useForm';
import { connect } from 'react-redux';
import * as actions from '../../actions/postComments';
import butterToast from '../ButterToast';
import { AssignmentTurnedIn } from '@material-ui/icons';

const initialFieldValues = {
    postId: '',
    message: ''
}

const CommentForm = (props) => {

    const validate = () => {
        let temp = { ...errors };
        console.log(temp);
        temp.message = values.message ? "" : "This field is required";

        setErrors({
            ...temp
        })
        return Object.values(temp).every(x => x === "");
    }

    var { values, setValues, errors, setErrors, handleInputChange, resetForm } = useForm(initialFieldValues, props.setCurrentId);

    const handleSubmit = e => {
        e.preventDefault();

        const onSuccess = () => {
            butterToast('Comment Box', 'New comment submitted.', <AssignmentTurnedIn />);
            resetForm()
        }

        if (validate()) {
            if (props.currentId === 0) {
                props.createComment(values, onSuccess);
            }
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <FormControl variant="outlined">
                <InputLabel>Select Post</InputLabel>
                <Select name="postId" label="Post" value={values.postId} onChange={handleInputChange} required>
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    {
                        (Object.keys(props.postList).length > 0) ? (
                            props.postList.map(post => {
                                return (
                                    <MenuItem key={post._id} value={post._id}>{post.title}</MenuItem>
                                )
                            })
                        ) : <MenuItem key='-1'>Empty</MenuItem>
                    }
                </Select>

                <TextField
                    name='message'
                    variant='outlined'
                    label='Message'
                    fullWidth
                    multiline
                    rows={4}
                    value={values.message}
                    onChange={handleInputChange}
                    required
                    {...(errors.message && { error: true, helperText: errors.message })}
                />

                <Button
                    variant='contained'
                    color='primary'
                    size='large'
                    type='submit'
                >Submit</Button>
            </FormControl>
        </form>
    );
}

const mapStateToProps = state => ({
    postList: state.postMessage.list,
})

const mapDispatchToProps = {
    createComment: actions.Commentcreate
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentForm);