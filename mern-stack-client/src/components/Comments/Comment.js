import React, { useEffect, useState, Fragment } from 'react';
import { connect } from 'react-redux';
import * as CommentActions from '../../actions/postComments';
import * as MessageActions from '../../actions/postMessage';
import CommentForm from './CommentForm';
import { Grid, List, ListItem, ListItemText, Typography, Divider, Button } from '@material-ui/core';
import butterToast from '../ButterToast';
import { DeleteSweep } from '@material-ui/icons';

const Comment = (props) => {
    const [currentId, setCurrentId] = useState(0);

    useEffect(() => {
        props.fetchAllComments();
        props.fetchAllPostMessages();
    }, [])

    const onDelete = (id, index) => {

        const onSuccess = () => {
            butterToast('Comment Box', 'Deleted successfully.', <DeleteSweep />);
        }

        if (window.confirm('Are you sure you want to delete ?')) {
            props.deleteComment(id, onSuccess, index)
        }
    }

    return (
        <Grid container>
            <Grid item xs={5}>
                <CommentForm {...{ ...props.postMessage, currentId, setCurrentId }} />
            </Grid>

            <Grid item xs={7}>
                <List>
                    {
                        props.comments.map((record, index) => {
                            return (
                                <Fragment key={index}>
                                    <ListItem>
                                        <ListItemText>
                                            <Typography variant='h5'>
                                                Post Title : {record.title}
                                            </Typography>
                                            <div className="mb-20">
                                                Post message : {record.message}
                                            </div>
                                            <div>
                                                {
                                                    (record.comments.length > 0) ? (
                                                        record.comments.map((record, counter) => {
                                                            return (
                                                                <div className="mb-20">
                                                                    <p className="inline" key={record._id} value={record._id}>{record.message}</p>
                                                                    <Button className="pull-right" variant='contained' color='secondary' size='small' onClick={() => onDelete(record._id, index)}> Delete comment </Button>
                                                                </div>
                                                            )
                                                        })
                                                    ) : <p key='1'>No Comments found.</p>
                                                }
                                            </div>
                                        </ListItemText>
                                    </ListItem>
                                    <Divider component='li' />
                                </Fragment>
                            )
                        })
                    }
                </List>
            </Grid>
        </Grid>
    );
}

const mapStateToProps = state => ({
    comments: state.postComments.commentlist,
    postMessageList: state.postMessage.list,
})

const mapDispatchToProps = {
    fetchAllComments: CommentActions.CommentfetchAll,
    fetchAllPostMessages: MessageActions.fetchAll,
    deleteComment: CommentActions.Commentremove
}

export default connect(mapStateToProps, mapDispatchToProps)(Comment);