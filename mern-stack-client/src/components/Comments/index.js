import React from 'react';
import '../../App.css';
import Comment from './Comment';
import { Container, AppBar, Typography, Button, Toolbar, IconButton, Divider } from '@material-ui/core'
import HomeOutlined from '@material-ui/icons/HomeOutlined';
import ButterToast, { POS_CENTER, POS_BOTTOM } from 'butter-toast';
import { NavLink } from "react-router-dom";

function App() {
    return (

        <div>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <NavLink to='/' className="font">
                            <HomeOutlined />
                        </NavLink>
                    </IconButton>
                    <Typography variant="h6" >
                        Comments
                    </Typography>
                </Toolbar>
            </AppBar>

            <div className="mt-50 mb-50">
                <NavLink to='/' className="nav-link">
                    <Button variant='contained' color='primary' size='small'> Go to Dashboard </Button>
                </NavLink>
            </div>

            <Divider />

            <Container maxWidth='lg'>
                <div className="mt-50">
                    <Comment />
                </div>
            </Container>

            <ButterToast position={{ vertical: POS_BOTTOM, horizontal: POS_CENTER }} />
        </div>

    );
}

export default App;
