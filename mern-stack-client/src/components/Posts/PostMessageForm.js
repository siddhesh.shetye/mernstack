import React, { useEffect } from 'react';
import { TextField, Button } from '@material-ui/core';
import useForm from '../useForm';
import { connect } from 'react-redux';
import * as actions from '../../actions/postMessage';
import butterToast from '../ButterToast';
import { AssignmentTurnedIn } from '@material-ui/icons';

const initialFieldValues = {
    title: '',
    message: ''
}

const PostMessageForm = (props) => {

    useEffect(() => {

        if (props.currentId !== 0) {
            setValues({
                ...props.postMessageList.find(x => x._id === props.currentId)
            })
            setErrors({})
        }

    }, [props.currentId])

    const validate = () => {
        let temp = { ...errors };
        temp.title = values.title ? "" : "This field is required";
        temp.message = values.message ? "" : "This field is required";

        setErrors({
            ...temp
        })
        return Object.values(temp).every(x => x === "");
    }

    var { values, setValues, errors, setErrors, handleInputChange, resetForm } = useForm(initialFieldValues, props.setCurrentId);

    const handleSubmit = e => {
        e.preventDefault();

        const onSuccess = () => {
            butterToast('Post Box', 'New post submitted.', <AssignmentTurnedIn />);
            resetForm()
        }

        if (validate()) {
            if (props.currentId === 0) {
                props.createPostMessages(values, onSuccess);
            } else {
                props.updatePostMessages(props.currentId, values, onSuccess);
            }
        }
    }

    return (
        <form autoComplete='off' noValidate onSubmit={handleSubmit}>
            <TextField
                name='title'
                variant='outlined'
                label='Title'
                fullWidth
                value={values.title}
                onChange={handleInputChange}
                {...(errors.title && { error: true, helperText: errors.title })}
            />

            <TextField
                name='message'
                variant='outlined'
                label='Message'
                fullWidth
                multiline
                rows={4}
                value={values.message}
                onChange={handleInputChange}
                {...(errors.message && { error: true, helperText: errors.message })}
            />

            <Button
                variant='contained'
                color='primary'
                size='large'
                type='submit'
            >Submit</Button>
        </form>
    );
}

const mapStateToProps = state => ({
    postMessageList: state.postMessage.list
})

const mapDispatchToProps = {
    createPostMessages: actions.create,
    updatePostMessages: actions.update
}

export default connect(mapStateToProps, mapDispatchToProps)(PostMessageForm);