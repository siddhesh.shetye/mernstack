import React, { useEffect, useState, Fragment } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/postMessage';
import PostMessageForm from './PostMessageForm';
import { Grid, List, ListItem, ListItemText, Typography, Divider, Button } from '@material-ui/core';
import butterToast from '../ButterToast';
import { DeleteSweep } from '@material-ui/icons';

const PostMessages = (props) => {
    const [currentId, setCurrentId] = useState(0);

    useEffect(() => {
        props.fetchAllPostMessages()
    }, [])

    const onDelete = id => {

        const onSuccess = () => {
            butterToast('Post Box', 'Deleted successfully.', <DeleteSweep />);
        }

        if (window.confirm('Are you sure you want to delete ?')) {
            props.deletePostMessage(id, onSuccess)
        }
    }

    return (
        <Grid container>
            <Grid item xs={5}>
                <PostMessageForm {...{ currentId, setCurrentId }} />
            </Grid>

            <Grid item xs={7}>
                <List>
                    {
                        props.postMessageList.map((record, index) => {
                            return (
                                <Fragment key={index}>
                                    <ListItem>
                                        <ListItemText>
                                            <Typography variant='h5'>
                                                {record.title}
                                            </Typography>
                                            <div>
                                                {record.message}
                                            </div>
                                            <div>
                                                <Button variant='contained' color='primary' size='small' onClick={() => setCurrentId(record._id)}> Edit </Button>
                                                <Button variant='contained' color='secondary' size='small' onClick={() => onDelete(record._id)}> Delete </Button>
                                            </div>
                                        </ListItemText>
                                    </ListItem>
                                    <Divider component='li' />
                                </Fragment>
                            )
                        })
                    }
                </List>
            </Grid>
        </Grid>
    );
}

const mapStateToProps = state => ({
    postMessageList: state.postMessage.list
})

const mapDispatchToProps = {
    fetchAllPostMessages: actions.fetchAll,
    deletePostMessage: actions.remove
}

export default connect(mapStateToProps, mapDispatchToProps)(PostMessages);