import React from 'react';
import '../../App.css';
import PostMessages from './PostMessages';
import { Container, AppBar, Typography, Button, IconButton, Toolbar, Divider } from '@material-ui/core'
import ButterToast, { POS_CENTER, POS_BOTTOM } from 'butter-toast';
import HomeOutlined from '@material-ui/icons/HomeOutlined';
import { NavLink } from "react-router-dom";

function App() {
    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <NavLink to='/' className="font">
                            <HomeOutlined />
                        </NavLink>
                    </IconButton>
                    <Typography variant="h6" >
                        Posts
                    </Typography>
                </Toolbar>
            </AppBar>

            <div className="mt-50 mb-50">
                <NavLink to='/' className="nav-link">
                    <Button variant='contained' color='primary' size='small'> Go to Dashboard </Button>
                </NavLink>
            </div>

            <Divider />

            <Container maxWidth='lg'>
                <div className="mt-50">
                    <PostMessages />
                </div>
            </Container>

            <ButterToast position={{ vertical: POS_BOTTOM, horizontal: POS_CENTER }} />
        </div>

    );
}

export default App;
