import React from 'react';
import ButterToast, { Cinnamon } from 'butter-toast';

const butterToast = (title, content, icon) => {
    ButterToast.raise({
        content: <Cinnamon.Crisp title={title}
            content={content}
            scheme={Cinnamon.Crisp.SCHEME_PURPLE}
            icon={icon}
        />
    })
}

export default butterToast;