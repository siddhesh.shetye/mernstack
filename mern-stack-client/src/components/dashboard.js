import React from 'react';
import { NavLink } from "react-router-dom";
import { Container, AppBar, Typography, Button, Toolbar, IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles, withStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));


const dashboard = (props) => {

    const { classes } = props;

    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>

            <Container maxWidth='lg' className="mt-50">
                <NavLink to='/posts' className="nav-link">
                    <Button variant='contained' color='primary' size='small'> Go to posts </Button>
                </NavLink>

                <NavLink to='/comments' className="nav-link">
                    <Button variant='contained' color='primary' size='small'> Go to comments </Button>
                </NavLink>
            </Container>
        </div>
    );
}

export default withStyles(useStyles)(dashboard);