import api from './api.js'

export const ACTION_TYPES = {
    COMMENT_CREATE: 'COMMENT_CREATE',
    COMMENT_DELETE: 'COMMENT_DELETE',
    COMMENT_FETCH_ALL: 'COMMENT_FETCH_ALL',
}

export const CommentfetchAll = () => dispatch => {
    api.postComments().CommentfetchAll()
        .then(res => {
            dispatch({
                type: ACTION_TYPES.COMMENT_FETCH_ALL,
                payload: res.data
            })
        })
        .catch(err => console.log(err))
}


export const Commentcreate = (data, onSuccess) => dispatch => {
    api.postComments().create(data)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.COMMENT_CREATE,
                payload: res.data
            })
            onSuccess()
        })
        .catch(err => console.log(err))
}

export const Commentremove = (id, onSuccess, index) => dispatch => {
    api.postComments().delete(id)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.COMMENT_DELETE,
                payload:  {id, index}
            })
            onSuccess()
        })
        .catch(err => console.log(err))
}